namespace Miasma
{
    public interface IInteractable
    {
        public string InteractionText{get;}

        void Interact(); //For single button press interactions
        float LongInteract(float timeAdded); //For interactions that require holding
        bool getActiveStatus();
    }
}
