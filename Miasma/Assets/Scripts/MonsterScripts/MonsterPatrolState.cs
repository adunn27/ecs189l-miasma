// Lots of help from tutorial by Sebastion Lague https://www.youtube.com/watch?v=jUdx_Nj4Xk0&t=211s

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterPatrolState : MonsterState
{
    private float WalkSpeed = 5f;
    private float TurnSpeed = 120f;
    private float Acceleration = 10f;
    private float WaitTime = 0f;
    public static Color ColorUndetected = Color.blue;

	public MonsterPatrolState(MonsterController monster) : base(monster) {}

    public override void Start()
    {
		Monster.NavAgent.speed = WalkSpeed;
        Monster.NavAgent.angularSpeed = TurnSpeed;
        Monster.NavAgent.acceleration = Acceleration;

        Vector3[] pathPoints = new Vector3[Monster.PatrolPath.childCount];
        for (int i = 0; i < Monster.PatrolPath.childCount; i++)
        {
            pathPoints[i] = Monster.PatrolPath.GetChild(i).position;
        }

        Monster.StartCoroutine(this.FollowPath(pathPoints));
    }

	public override void Update()
    {
		Monster.Spotlight.color = ColorUndetected;

        if (Monster.CanSeePlayer())
            Monster.SwitchState(new MonsterWaryState(Monster));
    }

	public override void Stop()
	{
		Monster.StopAllCoroutines();
	}

    private IEnumerator FollowPath (Vector3[] pathPoints)
    {
		// Find the closes point to start the Path.
		float shortestDist = float.MaxValue;
        int targetPointIndex = 0;
		for (int i = 0; i < pathPoints.Length; i++)
        {
            float distToPoint = new Vector3(pathPoints[i].x - Monster.transform.position.x, 0,
                                            pathPoints[i].z - Monster.transform.position.z).magnitude;
			if (distToPoint < shortestDist)
			{
				shortestDist = distToPoint;
				targetPointIndex = i;
			}
        }
        Vector3 targetPoint = pathPoints[targetPointIndex];

		Monster.NavAgent.SetDestination(targetPoint);

		Monster.NavAgent.isStopped = false;

        while (true)
        {
			if (Monster.NavAgent.remainingDistance <= 1f)
            {
                targetPointIndex = (targetPointIndex + 1) % pathPoints.Length;
                targetPoint = pathPoints[targetPointIndex];
                yield return new WaitForSeconds(WaitTime);
				Monster.NavAgent.SetDestination(targetPoint);
            }
            yield return null;
        }

    }

    private IEnumerator TurnToFace(Vector3 target)
    {
        Vector3 directionToTarget = (target - Monster.transform.position).normalized;
        float targetAngle = 90 - Mathf.Atan2(directionToTarget.z, directionToTarget.x) * Mathf.Rad2Deg;

        while (Mathf.Abs(Mathf.DeltaAngle(Monster.transform.eulerAngles.y, targetAngle)) > 0.05f)
        {
            float angle = Mathf.MoveTowardsAngle(Monster.transform.eulerAngles.y, targetAngle, TurnSpeed * Time.deltaTime);
            Monster.transform.eulerAngles = Vector3.up * angle;
            yield return null;
        }
    }
}