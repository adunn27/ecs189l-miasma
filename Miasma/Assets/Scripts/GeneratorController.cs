using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Miasma;
using UnityEngine.VFX;

public class GeneratorController : MonoBehaviour, IInteractable
{
    private bool isActive = true;
    private float remainFixingTime = 0.0f;
    [SerializeField]
    private float lengthOfInteraction;
    [SerializeField]
    private float currentDuration = 0.0f;
    [SerializeField]
    private Light generatorLight;
    [SerializeField]
    private GameObject SpinningGenerator;
    [SerializeField]
    private float speed = 50.0f;
    [SerializeField]
    private Vector3 direction = new Vector3(0, 1, 0);
    [SerializeField]
    private Material glowingMaterial;
    [SerializeField]
    private VisualEffect visualEffect;
    [SerializeField]
    private MeshRenderer GenSphere;

    [SerializeField]
    private HUBObjectivePanel HUBObjectivePanel;

    public GameObject UIControl;

    public string InteractionText{get;} = "Hold E to Boot Up Terminal";

    // Update is called once per frame
    void Update()
    {
        remainFixingTime = Mathf.Clamp(remainFixingTime-Time.deltaTime, 0, lengthOfInteraction);

        if (isActive == true)
        {
            SpinningGenerator.transform.Rotate(direction * speed * Time.deltaTime);
        }
    }

    public bool getActiveStatus()
    {
        return isActive;
    }

    public bool getFixingStatus()
    {
        return remainFixingTime > 0.0f;
    }

    public void Interact()
    {

    }

    public float LongInteract(float timeAdded)
    {
        currentDuration += timeAdded;
        remainFixingTime += timeAdded;
        //Debug.Log("Powering down generator");
        if (isActive == true && currentDuration >= lengthOfInteraction)
        {
            isActive = false;
            UIControl.GetComponent<UITerminalManager>().OpenTerminal(this);
        }
        return currentDuration/lengthOfInteraction;
    }

    public void ShutDownGenerator()
    {
        Debug.Log("Generator is powered down");
        glowingMaterial.SetFloat("ControlBoxGlowIntensity", 100);
        visualEffect.Stop();
        HUBObjectivePanel.DecreaseRemainingGenerators();
        generatorLight.enabled = false;
        GenSphere.material.shader = Shader.Find("Universal Render Pipeline/Terrain/Lit");
    }
}
