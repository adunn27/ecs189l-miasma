using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterPursuitState : MonsterState
{
   
    private float WalkSpeed = 9f;
    private float TurnSpeed = 130f;
    private float Acceleration = 10f;
    public static Color ColorDetected = Color.red;
    public MonsterPursuitState(MonsterController monster) : base(monster) {}
    private ToggleChasingBGM ChaseOn;
    private ToggleChasingBGM ChaseOff;
    private delegate void ToggleChasingBGM();
    private Coroutine ChaseCoroutine;

    public override void Start()
    {
        var BGMController = GameObject.FindGameObjectWithTag("Player").GetComponent<InGameBGM>();
        ChaseOn  = new ToggleChasingBGM(BGMController.ChasingOn);
        ChaseOff = new ToggleChasingBGM(BGMController.ChasingOff);
        ChaseOn();
        
        Monster.NavAgent.speed = WalkSpeed;
        Monster.NavAgent.angularSpeed = TurnSpeed;
        Monster.NavAgent.acceleration = Acceleration;
        
        Monster.NavAgent.SetDestination(Player.position);
        Monster.NavAgent.isStopped = false;
        ChaseCoroutine = Monster.StartCoroutine(this.ChasePlayer());
    }
    
    public override void Update()
    {
        if (!Monster.CanSeePlayer())
            Monster.SwitchState(new MonsterLostSightState(Monster));
        else if (Monster.NavAgent.remainingDistance <= 5f)
            Monster.SwitchState(new MonsterCaughtState(Monster));
    }

    public override void Stop()
    {
        ChaseOff();
        Monster.StopCoroutine(ChaseCoroutine);
        Monster.NavAgent.isStopped = true;
    }

    private IEnumerator ChasePlayer()
    {   
        while (true)
        {
            Monster.NavAgent.SetDestination(Player.position);
            yield return new WaitForSeconds(0.1f);
        }
    }
}
