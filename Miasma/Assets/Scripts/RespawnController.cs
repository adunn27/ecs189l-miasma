using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class RespawnController : MonoBehaviour
{
    [SerializeField]
    Image blackOutSquare;
    [SerializeField]
    GameObject Player;

    public int currentSpawn = 0;
    public GameObject[] Spawns;

    private AltarInteractable currentAltar;

    public void RespawnPlayer()
    {
        blackOutSquare.GetComponent<Image>().color = Color.black;
        StartCoroutine(FadeBlackOutSquare(false));
        Player.transform.position = Spawns[currentSpawn].transform.position;
    }

    public void SetRespawn(int newSpawn, AltarInteractable newAltar)
    {
        if (currentAltar != null)
        {
            currentAltar.TurnOff();
        }

        currentAltar = newAltar;
        currentSpawn = newSpawn;
    }

    public IEnumerator FadeBlackOutSquare(bool fadeToBlack = true, float fadeSpeed = 0.4f)
    {
        Color objectColor = blackOutSquare.GetComponent<Image>().color;
        float fadeAmount;

        if (fadeToBlack)
        {
            while (blackOutSquare.GetComponent<Image>().color.a < 1)
            {
                fadeAmount = objectColor.a + (fadeSpeed * Time.deltaTime);

                objectColor = new Color(objectColor.r, objectColor.g, objectColor.b, fadeAmount);
                blackOutSquare.GetComponent<Image>().color = objectColor;
                yield return null;
            }
        }
        else
        {
            while (blackOutSquare.GetComponent<Image>().color.a > 0)
            {
                fadeAmount = objectColor.a - (fadeSpeed * Time.deltaTime);

                objectColor = new Color(objectColor.r, objectColor.g, objectColor.b, fadeAmount);
                blackOutSquare.GetComponent<Image>().color = objectColor;
                yield return null;
            }
        }
    }
}
