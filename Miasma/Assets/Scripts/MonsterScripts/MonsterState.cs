using UnityEngine;

public abstract class MonsterState
{
	protected MonsterController Monster;
	protected Transform Player;

	public MonsterState(MonsterController monster)
	{
		this.Monster = monster;
		this.Player = monster.Player;
	}

	public abstract void Update();
	public virtual void Start() {}
	public virtual void Stop() {}
}