using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BGSoundEffect : MonoBehaviour
{
    private AudioSource audioSource;
    private IEnumerator coroutine;
    [SerializeField] private List<AudioClip> clips;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void Start()
    {
        coroutine = PlayRandomBGSE(0.0f);
        StartCoroutine(coroutine);
    }

    private IEnumerator PlayRandomBGSE(float timeInterval)
    {
        while(true)
        {
            timeInterval = Random.Range(180.0f,300.0f);
            yield return new WaitForSeconds(timeInterval);
            audioSource.PlayOneShot(clips[Random.Range(0,clips.Count)]);
        }
    }
}