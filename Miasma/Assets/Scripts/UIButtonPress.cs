using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIButtonPress : MonoBehaviour
{
    public bool Paused = false;
    [SerializeField]
    public GameObject FPAIO;
    public Canvas PauseUI;
    public Canvas OptionsUI;
    public Slider SensSlider;

    private void Start()
    {
        if (SceneManager.GetActiveScene().name == "Miasma")
        {
            PauseUI.enabled = false;
            OptionsUI.enabled = false;
        }
    }

    void Update()
    {

        if (SceneManager.GetActiveScene().name == "Miasma" && (Input.GetKeyDown("escape") || Input.GetKeyDown("p")))
        {

            if (!Paused)
            {
                Paused = !Paused;
                PauseUI.enabled = !PauseUI.enabled;
                Time.timeScale = 0;
                FPAIO.GetComponent<FirstPersonAIO>().ControllerPause();
            }
            else
            {
                Paused = !Paused;
                PauseUI.enabled = !PauseUI.enabled;
                Time.timeScale = 1;
                FPAIO.GetComponent<FirstPersonAIO>().ControllerPause();
            }

            if (OptionsUI.enabled)
            {
                OptionsUI.enabled = false;
            }
        }
    }

    public void SwapSceneButtonPress(string name)
    {
        Paused = false;
        Time.timeScale = 1;
        print("The new scene is: " + name);
        SceneManager.LoadSceneAsync(name);
    }

    public void ResumeButtonPress()
    {
        Paused = !Paused;
        PauseUI.enabled = !PauseUI.enabled;
        if (OptionsUI.enabled)
        {
            OptionsUI.enabled = false;
        }
        FPAIO.GetComponent<FirstPersonAIO>().ControllerPause();
        Time.timeScale = 1;
    }

    public void OptionsButtonPress()
    {
        OptionsUI.enabled = !OptionsUI.enabled;
        SensSlider.value = FPAIO.GetComponent<FirstPersonAIO>().mouseSensitivity;
    }

    public void SensitivitySliderUpdate()
    {
        FPAIO.GetComponent<FirstPersonAIO>().mouseSensitivity = SensSlider.value;
    }

    public void QuitButtonPress()
    {
        Application.Quit();
        Debug.Log("Quitting Miasma!");
    }
}
