using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITerminalManager : MonoBehaviour
{
    public Canvas TermScreen;
    public Image LogBox;
    public Image GenStatusBox;
    public GameObject FPAIO;
    public GameManager GameMngr;
    public UISceneManager SceneManager;
    public Button Log3;
    public Button Log4;
    public Button Log5;
    public Button Log6;
    
    private int LoginCount = 0;
    private int openLog;
    private GeneratorController CallerGen;

    public void Start()
    {
        TermScreen.enabled = false;
        CloseLog();

        openLog = -1;
    }

    public void OpenTerminal(GeneratorController Caller)
    {
        TermScreen.enabled = true;
        Time.timeScale = 0;
        FPAIO.GetComponent<FirstPersonAIO>().ControllerPause();

        CallerGen = Caller;

        LoginCount++;

        if (LoginCount > 1)
        {
            Log3.interactable = true;
            Log4.interactable = true;
        }

        if (LoginCount > 2)
        {
            Log5.interactable = true;
            Log6.interactable = true;
        }
    }

    public void OpenLog(int LogNum)
    {
        if (LogNum != openLog)
        {
            LogBox.enabled = true;
            LogBox.GetComponentInChildren<Text>().enabled = true;
            LogBox.GetComponentInChildren<Text>().text = SetText(LogNum);
            openLog = LogNum;
        }
        else
        {
            openLog = -1;
            CloseLog();
        }
    }

    public void CloseLog()
    {
        LogBox.enabled = false;
        LogBox.GetComponentInChildren<Text>().enabled = false;
    }

    private string SetText(int logNum)
    {
        string newText;
        switch (logNum)
        {
            case 0:
                newText = "Attention:\n\nDue to memory limits on the generators, log downloads will be divided between active terminals onsite.\n\nTo request a terminal receive a memory upgrade, fill out form 1138 and forward it to Terminal Help -Expansion.\n\nYour terminal will receive the requested maintenance in 4-6 weeks.";
                break;
            case 1:
                newText = "2-16-20XX\n\nI've arrived at the projected generator site. Beautiful place. The amount of open space here should do wonders for our testing. If this goes smoothly, there's no way the higher - ups can turn down our proposal again.\n\nConstruction will begin in a week, so long as the papers go through. Twenty grand in an unmarked envelope says they will.";
                break;
            case 2:
                newText = "2-27-20XX\n\nThe permits have finally come through, later than scheduled. Apparently, there was a delay while they discussed some old shrines scattered through the forest. We can build what we like, so long as we don't touch the shrines.\n\nIt shouldn't be a problem. The generators won't affect anything that's not alive. Still, I've asked one of the interns to research their significance. Always good to get ahead of any cultural backlash.";
                break;
            case 3:
                newText = "3-11-20XX\n\nThe generators are well on their way! With our current pace, we should have them up and running in a month or so.\n\nThe company delivered our new security yesterday. Some biotech hybrid fresh from R&D to keep trespassers out. Terrifying, really. I'll see if I can fit a fence in the budget. The last thing we need is some vagrant getting killed up here.";
                break;
            case 4:
                newText = "4-15-20XX\n\nThe project is complete, right on schedule! The generators should be ready to start now, but we'll wait a week for tech checks. Once we start them, not even we'll be allowed up here. Too many potential health risks.\n\nIf the power output of these generators match our predictions, we should have these being set up everywhere in the next few years.";
                break;
            case 5:
                newText = "4-19-20XX\n\nI was sorting through some papers and found that report I ordered on the shrines. I guess that intern was reliable. Shame I fired him.\n\nApparently, the shrines serve as a place to rest and meditate. \"Clear your mind, and accept your past.\" Hundreds of years old. As old as the forest.\n\nStill, I suppose progress marches on. ";
                break;
            case 6:
                newText = "4-22-20XX\n\nWell, the generators start tonight, and the site will be locked off. The next time anyone goes in, the forest will have died off. Hundreds of years, over in a few months.\n\nI suppose I might as well do some final checks of the generators, just in case.\n\nCould look at those shrines too, while I'm there.";
                break;
            default:
                newText = "Error; Data Corrupted!";
                break;
        }
        return newText;
    }

    public void ShutDown()
    {
        TermScreen.enabled = false;
        Time.timeScale = 1;
        FPAIO.GetComponent<FirstPersonAIO>().ControllerPause();

        CallerGen.ShutDownGenerator();
        if (LoginCount == 3)
        {
            SceneManager.startCutscene(1);
        }
    }
}
