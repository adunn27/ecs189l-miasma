using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Miasma;

public class DoorButton : MonoBehaviour, IInteractable
{
    public bool isPressed = false;
    public string InteractionText { get; } = "Press E to Activate Door";
    public bool getActiveStatus()
    {
        return true;
    }

    public void Interact()
    {
        isPressed = true;
    }

    public float LongInteract(float timeAdded)
    {
        return 0f;
    }
}
