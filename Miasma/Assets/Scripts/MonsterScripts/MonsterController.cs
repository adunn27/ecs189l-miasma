using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MonsterController : MonoBehaviour
{
    private MonsterState CurrentState;
    [SerializeField] public Transform PatrolPath;
    [SerializeField] public Light Spotlight;
    [SerializeField] public float ViewDistance;
    [SerializeField] public LayerMask ViewMask;
    [SerializeField] public NavMeshAgent NavAgent;
    [System.NonSerialized] public float ViewAngle;
    [System.NonSerialized] public Transform Player;

    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player").transform;
        ViewAngle = Spotlight.spotAngle;
        transform.position = PatrolPath.GetChild(0).position;
		NavAgent.isStopped = true;

        SwitchState(new MonsterPatrolState(this));
    }

    void Update()
    {
        if (CurrentState != null)
            CurrentState.Update();
    }

    public void SwitchState(MonsterState newState)
    {
        if (CurrentState != null)
            CurrentState.Stop();

        CurrentState = newState;

        if (CurrentState != null)
            CurrentState.Start();
    }

    void OnDrawGizmos()
    {
        Vector3 startPosition = PatrolPath.GetChild(0).position;
        Vector3 prevPosition = startPosition;

        foreach (Transform pathPoint in PatrolPath)
        {
            Gizmos.DrawSphere(pathPoint.position, 0.3f);
            Gizmos.DrawLine(prevPosition, pathPoint.position);
            prevPosition = pathPoint.position;
        }

        Gizmos.DrawLine(prevPosition, startPosition);

        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, transform.forward * ViewDistance);
    }

	public bool CanSeePlayer()
    {
		if (Vector3.Distance(this.transform.position, Player.position) < this.ViewDistance)
		{
			Vector3 dirToPlayer = (Player.position - this.transform.position).normalized;
			float angleToPlayer = Vector3.Angle(this.transform.forward, dirToPlayer);
			if ((angleToPlayer < this.ViewAngle / 2f) &&
				(!Physics.Linecast(this.transform.position, Player.position, this.ViewMask)))
			{
				return true;
			}
		}
		return false;
    }
}
