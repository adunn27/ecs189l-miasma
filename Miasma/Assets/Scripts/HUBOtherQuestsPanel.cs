using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HUBOtherQuestsPanel : MonoBehaviour
{
    [SerializeField]private GameObject TopLeftText;
	[SerializeField]private GameObject Panel;
	[SerializeField]private int MaxLine;

	private GameObject[] text;
	private int CurrentLine;

	// Start is called before the first frame update
    void Start()
    {
    	CurrentLine = 0;
		text = new GameObject[MaxLine];
        
    }

    /* 
    //test
    void Update()
    {
    	if(Input.GetButtonDown("Fire1"))
		{
			NewLine("Line test......");
			NewSubLine("SubLine test......");
		}
		if(Input.GetButtonDown("Fire2"))
		{
			DestroyLine(2);
			
		}
    }
    */


    public void NewLine(string message)
    {
    	if (CurrentLine >= MaxLine)
    	{
    		//Debug.Log("NewLine is at max limit: " + CurrentLine);
    		return;
    	}

    	text[CurrentLine] = _NewLine(message);
    }
    
    public void NewSubline(string message)
    {
    	if (CurrentLine >= MaxLine)
    	{
    		//Debug.Log("NewSubLine is at max limit: " + CurrentLine);
    		return;
    	}

    	text[CurrentLine] = _NewSubLine(message);
    }
    

    // index start from 1
    public void DestroyLine(int number)
    {
    	if (number > CurrentLine)
    	{
    		return;
    	}

    	number--;
    	Destroy(text[number]);
    	for (int line = number; (line + 1) < CurrentLine; line++)
    	{
    		text[line] = text[line + 1];
    	}
    	CurrentLine--;
    	//Debug.Log("DestroyLine: " + (number+1) + ", current:" + CurrentLine);
    }

    public bool DestroyByContent(string message)
    {
    	if (CurrentLine == 0)
    	{
    		return false;
    	}

    	for (int i = 0; i < CurrentLine; i++)
    	{
    		if (text[i].GetComponent<TextMeshProUGUI>().text == message)
    		{
    			Destroy(text[i]);
    			CurrentLine--;
    			for (int line = i; (line + 1) < CurrentLine; line++)
			    	{
			    		text[line] = text[line + 1];
			    	}
			    return true;
    		}
    	}
    	return false;
    }

    public void DestroyLastLine()
    {
    	CurrentLine--;
    	Destroy(text[CurrentLine]);
    	
    	//Debug.Log("Destroy the last line. Current:" + CurrentLine);
    }


    private GameObject _NewLine(string message)
    {	
    	var newL = (GameObject)Instantiate(TopLeftText, Panel.transform, false);
    	CurrentLine++;
        //newL.transform.SetParent(Panel.transform);
        newL.GetComponent<TextMeshProUGUI>().text = message;
        return newL;

    }
    private GameObject _NewSubLine(string message)
    {
    	var newL = (GameObject)Instantiate(TopLeftText, Panel.transform, false);
    	CurrentLine++;
        //newL.transform.SetParent(Panel.transform);
        newL.GetComponent<TextMeshProUGUI>().text = "\t" + message;
        return newL;
    }
}
