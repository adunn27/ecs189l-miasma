# ECS189L-Miasma

A first-person horror-stealth game developed in Unity as part of Spring 2021 UC Davis ECS 198L Game Programming class final. You can download the built version of the game at https://jeffk1m.itch.io/miasma

# REUPLOADED REPO NOTE:

This is a reupload of the previous repository at https://github.com/jeffk1m/ECS189L-Miasma. This repo was made using recovered folders on the team members local machines. Nearly everything was recovered except the finished README and possibly some last minute fixes. The following README is from an earlier draft and is missing certain sections, as well as some major and minor corrections/rewrites to the other sections. If you do have a later version of the README or any other later copy of the code saved somewhere please send it to the current maintainer, [Alex Dunn](https://gitlab.com/adunn27), thanks!

# Game Basic Information #

## Summary ##

**A paragraph-length pitch for your game.**

Miasma is an interactive first-person horror-stealth game that takes place in a serene forest that has been corrupted by an evil miasma produced by cursed generators. In this game you play as a mysterious person that has been tasked with shutting down the evil generators scattered throughout the forest while avoiding their dangerous robot guards. Explore the mystical forest, avoid the robotic guards, shut down the generators, discover the truth, and restore the forest in Miasma by Team Jasper.

## Gameplay Explanation ##

**In this section, explain how the game should be played. Treat this as a manual within a game. It is encouraged to explain the button mappings and the most optimal gameplay strategy.**

Controls:
- "WASD" to move the player
- "Mouse" to look around
- "E" to interact with interactables
- "P" to pause the game and turn on settings menu

Goal:
- Avoid the monsters and power down the three generators producing an evil miasma that is cursing the forest.
- The player starts in a short area that will introduce them to the mechanics of the game. After which is a hub area leading to three different areas each with its own generator and a unique theme, the player must disable the generator at the end of each area to complete the game.

Monsters:
- Monsters have a spotlight that reprsents their field of view and their current level of alertness. The player is unable to counter the robotic monsters' strength and thus must avoid all confrontation. While the monsters' lights are blue the player is still undetected and the monster will follow its set patrol route. When the monster first glimspes the player the light will become yellow and turn more red as it scans the player as a threat, it is reccomended you quickly escape this light before the monster fully detects the player. Once the light turns red the monster has locked onto the player and has begun its pursuit, it is advised that the players runs as fast as they can. 

Shrines:
- Light the candle on shrines in order to set the player's respawn point to the shrine.

Generators:
- To interact with the generators, the player must first find the terminal pannel and hold "E" to boot it up. Once the terminal is online, the player can read the files on the terminal and when they are read shut the generator down from the button near the bottom of the terminal.

**If you did work that should be factored in to your grade that does not fit easily into the proscribed roles, add it here! Please include links to resources and descriptions of game-related material that does not fit into roles here.**

# Main Roles #

Your goal is to relate the work of your role and sub-role in terms of the content of the course. Please look at the role sections below for specific instructions for each role.

Below is a template for you to highlight items of your work. These provide the evidence needed for your work to be evaluated. Try to have at least 4 such descriptions. They will be assessed on the quality of the underlying system and how they are linked to course content. 

*Short Description* - Long description of your work item that includes how it is relevant to topics discussed in class. [link to evidence in your repository](https://github.com/dr-jam/ECS189L/edit/project-description/ProjectDocumentTemplate.md)

Here is an example:  
*Procedural Terrain* - The background of the game consists of procedurally-generated terrain that is produced with Perlin noise. This terrain can be modified by the game at run-time via a call to its script methods. The intent is to allow the player to modify the terrain. This system is based on the component design pattern and the procedural content generation portions of the course. [The PCG terrain generation script](https://github.com/dr-jam/CameraControlExercise/blob/513b927e87fc686fe627bf7d4ff6ff841cf34e9f/Obscura/Assets/Scripts/TerrainGenerator.cs#L6).

You should replay any **bold text** with your relevant information. Liberally use the template when necessary and appropriate.

## User Interface

**Describe your user interface and how it relates to gameplay. This can be done via the template.**

Loading scene (De-Yi Huang): the start game button in the main menu triggers the loading scene, which is created to avoid the game to freeze while loading Miasma world. Loading scene consists of two elements: background image and a tip. The background image is the screenshot of Miasma, and the tip tells something about the game.

head-up display (De-Yi Huang):
- Design as a prefab: HUD was designed to be a prefab, so the work will be stored in HUBCanvas.prefab instead of Miasma’s hierarchy. This makes git collaboration easier. 
- Component design pattern: HUD is prepared to be utilized by other parts of the game. It applied the component design pattern, and the HUD initially consisted of two main panels: Top left quests panel and center interaction panel.
- Initially rough sketch: when the HUD was being created, we were not sure how much progress we could finish by the deadline. As the status-display only becomes meaningful after the corresponding part is finished, HUD was initially just a rough sketch. The rough functionalities were told to other members, and those functionalities would be refined after the corresponding part is finished in order to save time. 
- Top left quests panel: there are 2 parts for the top left panel: objective panel and quests panel. [Objective panel](https://github.com/jeffk1m/ECS189L-Miasma/blob/75a18368bd31d0e412002e2825b0ceb9e3631f02/Miasma/Assets/Scripts/HUBObjectivePanel.cs) always shows the main goal of the game on the screen, and it is used by the generator controller to update the number of remaining generators. The [quests panel](https://github.com/jeffk1m/ECS189L-Miasma/blob/47cc86a3274fed7ba7f2277b6b4dda930757ae8d/Miasma/Assets/Scripts/HUBOtherQuestsPanel.cs) is not being used and still in the rough state. It only contains some simple functions such as instantiating a new line, instantiating a new subline, destroying line by number, destroying line by content, and destroying the last line.
- Center interaction panel: there are also 2 parts for the center panel: interaction part and storyteller part. The [interaction part](https://github.com/jeffk1m/ECS189L-Miasma/blob/75a18368bd31d0e412002e2825b0ceb9e3631f02/Miasma/Assets/Scripts/HUBInteractionPanel.cs) was used by the interaction system. An interactable game object displays its tooltip when being looked at. There is also a slider for long interaction. The slider accumulates the progress of long interaction, so the player doesn’t restart powering down a generator after being interrupted by monsters. The [storyteller](https://github.com/jeffk1m/ECS189L-Miasma/blob/75a18368bd31d0e412002e2825b0ceb9e3631f02/Miasma/Assets/Scripts/HUBInteractionPanel.cs#L64) is still in a rough state. It only contains a single function to update its text. 



## Movement/Physics

**Describe the basics of movement and physics in your game. Is it the standard physics model? What did you change or modify? Did you make your movement scripts that do not use the physics system?**

## Animation and Visuals

**List your assets including their sources and licenses.**

Used Assets (Unity Store EULA)

Licesning: CC0
- https://www.adobe.com/devnet/author_bios/Mixamo.html

Licensing: https://unity3d.com/legal/as_terms?_gl=1*vewz58*_ga*MjMxNjMwMjgwLjE2MTYwMzQ3NDg.*_ga_1S78EFL1W5*MTYyMzE5OTIxNC4xNS4wLjE2MjMxOTkyMTQuNjA.&_ga=2.130568399.1197207274.1623199218-231630280.1616034748

- https://assetstore.unity.com/packages/2d/textures-materials/nature/grass-and-flowers-pack-1-17100
- https://assetstore.unity.com/packages/3d/vegetation/lowpoly-forest-pack-42578
- https://assetstore.unity.com/packages/3d/environments/fantasy/fantasy-forest-environment-free-demo-35361
- https://assetstore.unity.com/packages/3d/vegetation/trees/dream-forest-tree-105297
- https://assetstore.unity.com/packages/3d/environments/fantasy-landscape-103573
- https://assetstore.unity.com/packages/3d/vegetation/the-illustrated-nature-sample-161188
- https://assetstore.unity.com/packages/3d/characters/humanoids/sci-fi/free-biomechanical-mutant-166330
- https://assetstore.unity.com/packages/3d/vegetation/trees/polygon-nature-low-poly-3d-art-by-synty-120152
- https://assetstore.unity.com/packages/2d/textures-materials/sky/fantasy-skybox-free-18353
- https://assetstore.unity.com/packages/tools/input-management/first-person-all-in-one-135316

**Describe how your work intersects with game feel, graphic design, and world-building. Include your visual style guide if one exists.**

The visual style of our game was inspired by low poly flat shaded games such as Firewatch and Breath of the Wild. We first created a reference board of what our game should look and feel like and from there I compiled a list of assets that fit that look and feeling. I personally wanted to create a beautiful and intriguing forest that draws the player in, but I also needed to make the forest feel altered and terrifying. In order to do this I used a combination of dark lighting and post processing to give off the feeling of a corrupted forest.

In addition to prebuilt Unity Store Assets I also created some of my own assets which include the generators, altar steps, door and door button. I decided to create my own assets for these objects because they are main focal points that catch the player's eyes so I felt that it was needed for the player to find these objects interesting. I decided not to create my own nature assets since they're not the main focal point and it would take too long to create my own nature assets such as grass, trees and stones. Instead for nature assets I used Unity store assets packs.

Most of the time spent on the art side was level building. We had thought of an idea of a large level split up into three separate sections or sub levels. One area's theme would be blindness and claustrophobia, another area's theme would be confusing and maze like, and finally the last area's theme would be a large forest that the player would get lost in, throughout all of the levels the player would have to avoid enemies.

In addition to creating my own assets I also wanted to create some game juice by adding unique effects to the control box of the generator. The special effect I created was designed in VFX Graph and is a sparking particle effect. In addition I created a unique interactable glowing shader that is applied to the control box. This causes the player to understand that the "Control Box" is what the player should interact with. In addition, when the control box is interacted with and the generator is shut down the feeling of shutting down the generator is very satisfying as a strong sound is generated.

The robot walking animations were sourced from Mixamo. We went with mixamo because it's free to use, high quality and works with most rigs.

Since our game needed a specific level, I decided to sculpt the terrain by hand rather than using a pregenerated terrain map. I used Unity's Terrain System to lay out the terrain of the level by hand. I then used Unity's terrain tools to lay out Grass, Trees and Stones. The reason why we needed a custom built terrain was because we had a plan of three separate stages that are interconnected because of this reason we opted for a custom terrain. I also built out the specific levels for each of the stages. The most time consuming but fun part was the maze section since it required a custom maze to be layed out and I had to place each stone element by hand to make sure the maze looked good.

## Input

(De-Yi Huang & Jeffrey Kim)


**Describe the default input configuration.**

**Add an entry for each platform or input style your project supports.**


- [First Person All-in-One asset](https://github.com/jeffk1m/ECS189L-Miasma/tree/main/Miasma/Assets/FirstPersion%20AIO%20Pack): For our input system we went with a basic first person input system that is familiar to everyone such as WASD movement, E for interaction, Space for jumping and CTRL for crouching. The basic movement options were pre set up with the [First Person All-in-One asset pack](https://assetstore.unity.com/packages/tools/input-management/first-person-all-in-one-135316). Custom code was written for the interaction system that the player uses to interact with game objects.

Currently we only support PC Mouse and Keyboard for Input System:
- WASD for basic character movement
- Space for jumping
- Ctrl for crouching
- Shift to speed up
- Press/Hold E to interact with items and objects
- "P" to pause the game and turn on the settings menu


Custom interaction system:

- Interaction system: The interaction system is based on the factory design pattern. It has an underlying interface [`IInteractable.cs`](https://github.com/jeffk1m/ECS189L-Miasma/blob/75a18368bd31d0e412002e2825b0ceb9e3631f02/Miasma/Assets/Scripts/IInteractable.cs) and a script [`SelectionManager.cs`](https://github.com/jeffk1m/ECS189L-Miasma/blob/75a18368bd31d0e412002e2825b0ceb9e3631f02/Miasma/Assets/Scripts/SelectionManager.cs) to apply the interface. On top of the interface, there are 2 scripts, [`GeneratorController.cs`](https://github.com/jeffk1m/ECS189L-Miasma/blob/75a18368bd31d0e412002e2825b0ceb9e3631f02/Miasma/Assets/Scripts/GeneratorController.cs) and [`AltarInteractable.cs`](https://github.com/jeffk1m/ECS189L-Miasma/blob/75a18368bd31d0e412002e2825b0ceb9e3631f02/Miasma/Assets/Scripts/AltarInteractable.cs), that were built and used to store their unique implementations of the interaction system. 
- IInteractable.cs: This script creates interfaces that enrich the user experience. The two methods are long interaction and instant interaction. The long interaction requires player to hold E, and the instant interaction happens at the moment player presses E. Also, there is another function `getActiveStatus()` to get the permission of interaction.
  * [tooltip of interaction](https://github.com/jeffk1m/ECS189L-Miasma/blob/75a18368bd31d0e412002e2825b0ceb9e3631f02/Miasma/Assets/Scripts/GeneratorController.cs#L33): `IInteractable.cs` contains a string `InteractionText` to guide players to interact with different game objects.
- Generator Controller: The generator controller requires that the player holds "E" on the controller for a few seconds to trigger the interact function. If the time to power down is too short then the tension loses, but if the time is too long then the process becomes boring as it needs to repeat 3 times. We designed it as a 10 second process at the end.  [lengthOfInteraction is a SerializeField and is set on hierarchy](https://github.com/jeffk1m/ECS189L-Miasma/blob/75a18368bd31d0e412002e2825b0ceb9e3631f02/Miasma/Assets/Scripts/GeneratorController.cs#L12)


## Game Logic

**Document what game states and game data you managed and what design patterns you used to complete your task.**

# Sub-Roles

## Audio

**List your assets including their sources and licenses.**
No any additional asset is used for the audio in this game. All of the music and sound effects we use are copyright free. These are the two website
where we obtain the raw audio clip:
- [Epidemic](https://www.epidemicsound.com/)
- [Uppbeat](https://uppbeat.io/)

**Describe the implementation of your audio system.**
We mainly catagorize the audio into five main groups:
- Footsteps (Player and Monster)
- BGM (In game Normal and Chasing) and [`fading out`](https://github.com/jeffk1m/ECS189L-Miasma/blob/4e9430f97c54c3ac03bd6361b1939da952fbf545/Miasma/Assets/Audio/Scripts/InGameBGM.cs#L96) when switch between these two types of the BGM
- Background sound effect (Bird squwak and Wind blow)
- Interaction (Generating Fixing, PowerDown, Altar lighting up, cradle)
- Player sound effect (heavy breath when out-of-breath).

All the raw audio clips we obtain must be processed for the use in the project, which mean, they have to be amplified/quietened, 
to have their tempo being changed, or generate silence when necessary. For example,
- Player footsteps, originally 1 min human footsteps clip, seperated into 27 single footstep audio clip, each with 0.32s duration.
- Monster footsteps, originally having a tempo slower than the monster animation, thus need to have its tempo increased to sync the animation.

**Document the sound style.** 
We avoid using text-to-speech software to generate human voice, given that most of these software require payment, while the free ones 
have voice too cartoony nor doesn't have the voice we want.

We aim at attempting to implement a sound style which tighten the atmosphere, making our player feel more stressed/intensed. This is a key
element in a successful horror game. We intend to accomplish this by using some horror, chasing BGM. Also, we try to focus on editting the 
player footsteps quite a lot, since in a horror game listen to your own footsteps make you feel like the surrounding is really silent and
you are alone, having to take the challenge by yourself. We also try to make the monster footsteps really intense and make some background
bird scary voice.

There is many regrets in the audio system, i.e. the Monster Footsteps kinda weird. I personally try and believe this is an issue which
can hardly fix by using a audio edit software, since the clip has its voice changed when its tempo and pitch changed at the meantime. 
That monster footstep clip would probably be the best one we can find and priceless. However, we still try to make the audio as real
as possible. 

## Gameplay Testing

**Summarize the key findings from your gameplay tests.**
During gameplay testing most of our play testers which included our friends thought that the game was tense and the graphics looked beautiful but the AI needed tweaking to be more of a challenge. We decided that the AI needed some more drive so we changed some attributes of the artificial intelligence to make the AI much more aggressive and pursue the player for longer. This really increased the tension when a player got caught. Originally the AI would lose interest in the player very quickly but with this change the AI would chase the player more and cause the player to feel more tense.

## Narrative Design

**Document how the narrative is present in the game via assets, gameplay systems, and gameplay.** 

There are two major ways that the story is presented to the player (outside of just the games atmosphere.) The firat is the initial and closing cutscenes, found by interacting with the shrine at the start of the game and by turning off the final generator at the end. These serve as the driving force for the player, explaining the premise and wrapping up the game.

The other aspect is the employee logs, found on each generator. As you reach each generator, more logs unlock, giving the history of the generators, the forest, and of the employee writing them.

<details>
  <summary>Story Summary</summary>
  Towards the tail end of the logs, the employee begins to show slight traces of some regret over what he's doing. As a result, he travels to the generator site on the last evening before he is restricted from doing so. The implication is that the player is the employee, shutting down his own work. This is what the starting and ending cutscenes refer to, with the need to atone and come to terms with your past.
</details>

## Press Kit and Trailer

**Include links to your presskit materials and trailer.**

Press Kit (De-Yi Huang): https://docs.google.com/document/d/1F-7B0RcRtNm4OdijFbrUZBdLvr9DokfWqRhy5LstCy4/edit#

- Style consistency: in order to match the style of the press kit with the game, the image used in Main Menu is applied as decoration. The font and background color are also chosen on purpose.
- Elements in presskit: Description introduces the basic mechanism of the game. Miasma Story introduces the identity of the player and enhances the immersion. Images visually show the game's features.

Trailer (Wing Lo): https://drive.google.com/file/d/1b2pXMPX8n_d7NznZ-IfGddYYzfWBLviC/view?usp=sharing

**Describe how you showcased your work. How did you choose what to show in the trailer? Why did you choose your screenshots?**
The graphic is a very powerful part in our game. I believe it may be the best to simply demonstate some of the beautiful landscape of the game.
The key purpose of a game trailer is to demonstrate the strength and attractiveness of your game, while telling the other how does the game 
look like, how is the atmosphere of the game. 

Most commerical use game trailer look like a movie where characters inside the game chat with each other, hinting some of the logic of the game.
It is of a great pity that we don't find the human voice generated from free text-to-speech software. I then simply decide to use our theme BGM --
In the Enemy's Eye. Just to generate a dark atmosphere, hinting to the audience that this is a horror game. (or probably related)

Some of the landscape are symbolic of the game, the generator, the altar, the cradle and the terminal. They are key components which build up the 
main game logic. We are hinting them to the audience.

I mean, when I am making the trailer, I don't really have a plan. I select those screenshots because I think they should be in the movie. They
build up the intense atmosphere of the game. To me, rather than being as a game trailer, I think it is more like how do I feel the game. 

## Game Feel

**Document what you added to and how you tweaked your game to improve its game feel.**
In order to improve game feel I added game juice to the game interactables. Some examples of this are listed below. In addition game feel was heavily tied into the Animations and Visuals section.
- Making Powering Down the Generator feel substantial
  * In order to make powering down the generator feel substantial and make the player feel as if they are progressing the game I decided to add some eye catching visual effects that would be turned off when the player finishes interacting with the generator. An example of this is a glowing effect that catches the players eye and would be turned off when the player finishes interacting with the generator. This would tlet the player know that they have complted interacting with the generator. In addition, the player will also hear a powerful sound letting the player know that the generator has been turend off.
- Making interacting with the doors feel interesting
  * In order to make opening the ancient doors feel interesting I added a unique animation that will play when the door is activated. In addition the door will play a powerful rumbling sound giving the player a feeling that the level is substantially changing.
- Adding extra visual flair to the generators
  * I also added extra visual flair to the generators such as a glowing light that eminates from the top of the generator that creates an uncanny evil looking feeling to the generator. 
  * In addition, I made the generator spin in order to give off the feeling that the generator is actually generating. When the player powers down the generator the spinning generator will stop spinning.
  * Shaders and Particles were added to the Control Box to notify the player of the generators status.
- Creating a claustrophobic feeling with post processing
  * I also created a claustrophobic feeling for the player when the player enters a unique zone such as the blindness tall grass area. This area causes the player to feel claustrophobic using a variety of post processing effects such as vignettes, exposures and color shifting. I learned that these unique effects cause  the player to feel as if something bad might happen and really heighten their sense of fear.
- Creating a Creepy Feeling to the forest
  * In order to create a creepy feeling to the forest that makes the player feel on edge I decided to use heavy post processing such as vignettes and increase the fog to reduce player visibility. This makes the player feel vulnerable and unknowing of what comes next.
