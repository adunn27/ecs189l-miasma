using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterWaryState : MonsterState
{
    private float DetectionTime = 1f;
    private float DetectionCounter;
    private float CooloffTime = 0.5f;
    private float TurnSpeed = 125f;
    private float CooloffCounter;
    public static Color ColorCautious = Color.yellow;

	public MonsterWaryState(MonsterController monster) : base(monster) {}

    public override void Start()
    {
        DetectionCounter = 0f;
        CooloffCounter = 0f;
        Monster.NavAgent.angularSpeed = TurnSpeed;
    }

	public override void Update()
    {
		Monster.Spotlight.color = Color.Lerp(ColorCautious, MonsterPursuitState.ColorDetected, DetectionCounter / DetectionTime);

        if (Monster.CanSeePlayer())
        {
			TurnToFace(Player.position);
        	CooloffCounter = 0f;
            DetectionCounter += Time.deltaTime;
            if (DetectionCounter >= DetectionTime)
                Monster.SwitchState(new MonsterPursuitState(Monster));
        }
        else if (DetectionCounter > 0)
        {
            DetectionCounter -= Time.deltaTime;
            if (DetectionCounter < 0)
                DetectionCounter = 0;
        }
		else
		{
			CooloffCounter += Time.deltaTime;
            if (CooloffCounter >= CooloffTime)
                Monster.SwitchState(new MonsterPatrolState(Monster));
			Monster.Spotlight.color = Color.Lerp(ColorCautious, MonsterPatrolState.ColorUndetected, CooloffCounter / CooloffTime);
		}
    }

     public override void Stop(){}

	void TurnToFace(Vector3 target)
    {
        Vector3 directionToTarget = (target - Monster.transform.position).normalized;
        float targetAngle = 90 - Mathf.Atan2(directionToTarget.z, directionToTarget.x) * Mathf.Rad2Deg;

		if ((Mathf.Abs(Mathf.DeltaAngle(Monster.transform.eulerAngles.y, targetAngle)) < 0.05f))
			return;

		float angle = Mathf.MoveTowardsAngle(Monster.transform.eulerAngles.y, targetAngle, TurnSpeed * Time.deltaTime);
		Monster.transform.eulerAngles = Vector3.up * angle;
    }
}