using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HUBObjectivePanel : MonoBehaviour
{
	[SerializeField]private GameObject CurrentObjective;
	[SerializeField]private GameObject RemainingGenerators;
	[SerializeField]private int NumOfGenerators;

	private int ActiveGenerators;

	void Start()
	{
		CurrentObjective.GetComponent<TextMeshProUGUI>().text = "";
		RemainingGenerators.GetComponent<TextMeshProUGUI>().text = "";
		ActiveGenerators = NumOfGenerators;
		InitializeCurrentObjectiveAndRemainingGenerator();
		//SetStoryText("display center text.");
	}


	public void InitializeCurrentObjectiveAndRemainingGenerator()
	{
		CurrentObjective.GetComponent<TextMeshProUGUI>().text = "Current Objective: deactivate the generator";
		RemainingGenerators.GetComponent<TextMeshProUGUI>().text = "Remaining Generator: " + NumOfGenerators;
	}

	public void UpdateCurrentObjective(string message)
	{
		CurrentObjective.GetComponent<TextMeshProUGUI>().text = "Current Objective: " + message;
	}

	public void DecreaseRemainingGenerators()
	{
		ActiveGenerators--;
        RemainingGenerators.GetComponent<TextMeshProUGUI>().text = "Remaining Generator: " + ActiveGenerators;
	}
}
