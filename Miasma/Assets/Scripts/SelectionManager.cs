using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Miasma;
public class SelectionManager : MonoBehaviour
{
    [SerializeField] private string selectableTag = "Selectable";
    [SerializeField] private string longSelectableTag = "LongSelectable";

    [SerializeField] private HUBInteractionPanel InteractionPanel;
    [SerializeField] private Camera cam;

    // Update is called once per frame
    void Update()
    {
        //var ray = Camera.main.ScreenPointToRay(cam.transform.forward);
        RaycastHit hit;
        if(Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, 8f))
        {
            var selection = hit.transform;
            //Debug.Log("Raycast: " + hit.collider.gameObject.tag);
            // Selectable
            if (selection.CompareTag(selectableTag))
            {
                //Debug.Log("selectableTag");
                var interactable = selection.GetComponent<IInteractable>();
                InteractionPanel.SetInteractiveText(interactable.InteractionText);
                //Debug.Log(interactable.InteractionText);
                if (Input.GetKeyDown(KeyCode.E))
                {
                    interactable.Interact();
                }
            }

            // LongSelectable
            else if (selection.CompareTag(longSelectableTag))
            {
                //Debug.Log("LongSelectable");
                var interactable = selection.GetComponent<IInteractable>();
                if (interactable.getActiveStatus())
                {
                    //var lengthOfInteraction = interactable.GetLengthOfInteraction();
                    //InteractionPanel.SetMaxOfProgressBar(lengthOfInteraction);
                    InteractionPanel.SetInteractiveText(interactable.InteractionText);
                    if (Input.GetKeyDown(KeyCode.E))
                    {
                        InteractionPanel.ActivateProgressBar();
                    }
                    if (Input.GetKey(KeyCode.E))
                    {
                        InteractionPanel.UpdateProgressBar(interactable.LongInteract(Time.deltaTime));
                    }
                    if (Input.GetKeyUp(KeyCode.E))
                    {
                        InteractionPanel.DeactivateProgressBar();
                    }
                }
            }
            else
            {
                InteractionPanel.ResetUI();
            }
        }
    }
}
