using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UISceneManager : MonoBehaviour
{
    public Image WhiteOut;
    public Text Line1;
    public Text Line2;
    public Text Line3;
    public Text KeyPrompt;
    public FirstPersonAIO FPAIO;
    public DoorInteractable Door;
    public UIButtonPress UIPress;
    public GameObject topLeft;
    public GameObject center;

    private string[][] LineStorage;
    private string[] Lines;
    private int LineNum;
    private int curLine = 0;
    private int stage = 0;
    private int currentScene;

    // Start is called before the first frame update
    void Start()
    {
        WhiteOut.GetComponent<Image>().color = new Color(1,1,1,0);
        Line1.GetComponent<Text>().color = new Color(0, 0, 0, 0);
        Line2.GetComponent<Text>().color = new Color(0, 0, 0, 0);
        Line3.GetComponent<Text>().color = new Color(0, 0, 0, 0);
        KeyPrompt.GetComponent<Text>().color = new Color(0, 0, 0, 0);
        setLines();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void startCutscene(int cutSceneNumber)
    {
        currentScene = cutSceneNumber;
        Lines = LineStorage[cutSceneNumber];
        LineNum = Lines.Length;
        stage = 0;
        curLine = 0;
        center.SetActive(false);

        StartCoroutine(RunCutscene());
    }

    public void endCutscene()
    {
        StopCoroutine(RunCutscene());
        center.SetActive(true);
        if (currentScene == 0)
        {
            topLeft.SetActive(true);
            Door.OpenDoor();
        }
    }

    public IEnumerator RunCutscene()
    {
        Color BackgroundColor = WhiteOut.GetComponent<Image>().color;
        Color Line1Color = Line1.GetComponent<Text>().color;
        Color Line2Color = Line2.GetComponent<Text>().color;
        Color Line3Color = Line3.GetComponent<Text>().color;
        Color PromptColor = KeyPrompt.GetComponent<Text>().color;
        float fadeAmount;
        bool waitForInput = false;
        bool done = false;

        while (!done)
        {
            if (stage == 0)
            {
                while (WhiteOut.GetComponent<Image>().color.a < 1)
                {
                    fadeAmount = BackgroundColor.a + (0.5f * Time.deltaTime);

                    BackgroundColor = new Color(BackgroundColor.r, BackgroundColor.g, BackgroundColor.b, fadeAmount);
                    WhiteOut.GetComponent<Image>().color = BackgroundColor;
                    yield return null;
                }
                if (curLine < LineNum)
                {
                    stage = 1;
                }
                else
                    stage = 6;
            }

            if (stage == 1)
            {
                Line1.text = Lines[curLine];
                curLine++;
                while (Line1.GetComponent<Text>().color.a < 1)
                {
                    fadeAmount = Line1Color.a + (0.7f * Time.deltaTime);

                    Line1Color = new Color(Line1Color.r, Line1Color.g, Line1Color.b, fadeAmount);
                    Line1.GetComponent<Text>().color = Line1Color;
                    yield return null;
                }
                if (curLine < LineNum)
                {
                    stage = 2;
                }
                else
                    stage = 4;
            }

            if (stage == 2)
            {
                Line2.text = Lines[curLine];
                curLine++;
                while (Line2.GetComponent<Text>().color.a < 1)
                {
                    fadeAmount = Line2Color.a + (0.7f * Time.deltaTime);

                    Line2Color = new Color(Line2Color.r, Line2Color.g, Line2Color.b, fadeAmount);
                    Line2.GetComponent<Text>().color = Line2Color;
                    yield return null;
                }
                if (curLine < LineNum)
                {
                    stage = 3;
                }
                else
                    stage = 4;
            }

            if (stage == 3)
            {
                Line3.text = Lines[curLine];
                curLine++;
                while (Line3.GetComponent<Text>().color.a < 1)
                {
                    fadeAmount = Line3Color.a + (0.7f * Time.deltaTime);

                    Line3Color = new Color(Line3Color.r, Line3Color.g, Line3Color.b, fadeAmount);
                    Line3.GetComponent<Text>().color = Line3Color;
                    yield return null;
                }
                stage = 4;
            }

            if (stage == 4)
            {
                while (KeyPrompt.GetComponent<Text>().color.a < 1)
                {
                    fadeAmount = PromptColor.a + (0.8f * Time.deltaTime);

                    PromptColor = new Color(PromptColor.r, PromptColor.g, PromptColor.b, fadeAmount);
                    KeyPrompt.GetComponent<Text>().color = PromptColor;
                    yield return null;
                }
                waitForInput = true;
                while (waitForInput)
                {
                    if (Input.anyKey)
                    {
                        stage = 5;
                        waitForInput = false;
                    }
                    yield return null;
                }
            }

            if (stage == 5)
            {
                while (KeyPrompt.GetComponent<Text>().color.a > 0)
                {
                    fadeAmount = PromptColor.a - (0.8f * Time.deltaTime);

                    PromptColor = new Color(PromptColor.r, PromptColor.g, PromptColor.b, fadeAmount);
                    if(Line1.GetComponent<Text>().color.a > 0)
                    {
                        Line1Color = PromptColor;
                        Line1.GetComponent<Text>().color = PromptColor;
                    }
                    if (Line2.GetComponent<Text>().color.a > 0)
                    {
                        Line2Color = PromptColor;
                        Line2.GetComponent<Text>().color = PromptColor;
                    }
                    if (Line3.GetComponent<Text>().color.a > 0)
                    {
                        Line3Color = PromptColor;
                        Line3.GetComponent<Text>().color = PromptColor;
                    }
                    KeyPrompt.GetComponent<Text>().color = PromptColor;
                    yield return null;
                }
                if (curLine < LineNum)
                {
                    stage = 1;
                }
                else if (currentScene == 1)
                    UIPress.SwapSceneButtonPress("MainMenu");
                else
                    stage = 6;
            }

            if (stage == 6)
            {
                while (WhiteOut.GetComponent<Image>().color.a > 0)
                {
                    fadeAmount = BackgroundColor.a - (0.5f * Time.deltaTime);

                    BackgroundColor = new Color(BackgroundColor.r, BackgroundColor.g, BackgroundColor.b, fadeAmount);
                    WhiteOut.GetComponent<Image>().color = BackgroundColor;
                    yield return null;
                }
                done = true;
            }
            yield return null;
        }
        endCutscene();
        yield return null;
    }

    private void setLines()
    {
        LineStorage = new string[2][];
        string[] Scene0 = new string [11];
        string[] Scene1 = new string [11];

        Scene0[0] = "YOU";
        Scene0[1] = "STRANGER";
        Scene0[2] = "I HAVE NEED OF YOU";
        Scene0[3] = "THIS FOREST HAS BEEN DESECRATED";
        Scene0[4] = "MONSTROUS MACHINES";
        Scene0[5] = "DRAINING THE FOREST OF LIFE";
        Scene0[6] = "YOU MUST DESTROY THESE GENERATORS";
        Scene0[7] = "THEN ALL WILL BE AT PEACE";
        Scene0[8] = "YOU WILL BE AT PEACE";
        Scene0[9] = "GO NOW";
        Scene0[10] = "I WILL CALL WHEN IT IS DONE";

        Scene1[0] = "IT IS DONE";
        Scene1[1] = "I THANK YOU";
        Scene1[2] = "THE FOREST THANKS YOU";
        Scene1[3] = "YOU HAVE DONE WELL";
        Scene1[4] = "YOU HAVE ATONED FOR YOUR PAST";
        Scene1[5] = "PERHAPS YOU HAVE FOUND PEACE?";
        Scene1[6] = "NOW I MUST GO";
        Scene1[7] = "ALL I AM...";
        Scene1[8] = "...IS WHAT YOU BROUGHT WITH YOU";
        Scene1[9] = "";
        Scene1[10] = "THE END";

        LineStorage[0] = Scene0;
        LineStorage[1] = Scene1;
    }
}
