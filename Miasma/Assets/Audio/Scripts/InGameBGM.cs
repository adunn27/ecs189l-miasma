using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InGameBGM : MonoBehaviour
{
    [SerializeField] private List<AudioClip> clips;
    [SerializeField] private AudioClip ChasingBGM;
    private AudioSource audioSource;
    private List<int> played = new List<int>();
    private AudioClip BGM;
    private Coroutine normalCoroutine;
    private Coroutine chaseCoroutine;
    private int numMonsterChasing;
    private bool chaseCoroutineRunning = false;

    private void Awake()
    {
        var aSources = GetComponents<AudioSource>();
        audioSource = aSources[1];
    }

    private void Start()
    {
        normalCoroutine = StartCoroutine(PlayNormalBGM(false));
    }

    private IEnumerator PlayNormalBGM(bool fade = true)
    {
        if (fade)
            yield return FadeOut();

        while (true)
        {
            GetNormalBGM();
            var playTime = BGM.length; 
            audioSource.clip = BGM;
            audioSource.Play();
            yield return new WaitForSeconds(playTime);
        }
    }

    private IEnumerator PlayChaseBGM(bool fade = true)
    {
        if (fade)
            yield return FadeOut();

        while (true)
        {
            var playTime = ChasingBGM.length;
            audioSource.clip = ChasingBGM;
            audioSource.Play();
            yield return new WaitForSeconds(playTime);
        }
    }

    private void GetNormalBGM()
    {
        while (true)
        {
            var select = Random.Range(0, clips.Count);
            if (!played.Contains(select))
            {
                played.Add(select);
                BGM = clips[select];

                if (played.Count == clips.Count) { played = new List<int>(); }
                break;
            }
        }
    }

    public void ChasingOn()
    {
        numMonsterChasing++;
        if (!chaseCoroutineRunning)
        {
            chaseCoroutineRunning = true;
            StopCoroutine(normalCoroutine);
            chaseCoroutine = StartCoroutine(PlayChaseBGM());
        }
    }

    public void ChasingOff()
    {
        if (numMonsterChasing>0) { numMonsterChasing--; }
        if (numMonsterChasing == 0 && chaseCoroutineRunning)
        {
            chaseCoroutineRunning = false;
            StopCoroutine(chaseCoroutine);
            normalCoroutine = StartCoroutine(PlayNormalBGM());
        }
    }


    private IEnumerator FadeOut()
    {
        float original = audioSource.volume;

        while (audioSource.volume > 0)
        {
            audioSource.volume = Mathf.Clamp(audioSource.volume-Time.deltaTime/1.5f, 0, 1);
            yield return null;
        }
        audioSource.Stop();
        audioSource.volume = original;
        yield break;
    }
}