using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterCaughtState : MonsterState
{
    public MonsterCaughtState(MonsterController monster) : base(monster) {}

	public override void Start()
	{
		Monster.StartCoroutine(this.TriggerPlayerDeath());
	}

	public override void Update() {}

    private IEnumerator TriggerPlayerDeath() 
	{
        var PlayerControls = GameObject.FindGameObjectWithTag("Player").GetComponent<FirstPersonAIO>();
        var oldSensitivity = PlayerControls.mouseSensitivity;
        Player.LookAt(Monster.transform);
        
        PlayerControls.enableCameraMovement = false;

        //yield return new WaitForSeconds(1);

        var gameManager = GameObject.Find("GameManager");
        RespawnController res = gameManager.GetComponent<RespawnController>();
        res.RespawnPlayer();

        PlayerControls.enableCameraMovement = true;
        Monster.SwitchState(new MonsterPatrolState(Monster));
        yield return null;
    }
}