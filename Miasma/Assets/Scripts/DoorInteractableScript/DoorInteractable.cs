using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Miasma;
public class DoorInteractable : MonoBehaviour
{
    [SerializeReference] private Animator animator;
    [SerializeReference] private GameObject buttonGameObject;
    [SerializeReference] private AudioSource rumblingSound;
    private DoorButton button;
    private bool doorOpened = false;
    // Start is called before the first frame update
    void Start()
    {
        button = buttonGameObject.GetComponent<DoorButton>();
    }

    // Update is called once per frame
    void Update()
    {
        if (button.isPressed == true && doorOpened == false)
        {
            OpenDoor();
        }
    }

    public void OpenDoor()
    {
        animator.SetBool("isOpened", true);
        if (!rumblingSound.isPlaying)
        {
            rumblingSound.Play();
        }
        doorOpened = true;
    }
}
