using UnityEngine;
using System.Collections;

public class HeavyBreath : MonoBehaviour
{
    private AudioSource audioSource;
    private IEnumerator coroutine;
    private FirstPersonAIO fp;
    private float staminaInternal;
    private bool isPlaying = false;
    [SerializeField] private AudioClip clip;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        coroutine = PlayHeavyBreath();
        fp = GameObject.FindGameObjectWithTag("Player").GetComponent<FirstPersonAIO>();
    }

    private void Update()
    {
        staminaInternal = fp.staminaInternal;
        if (staminaInternal < 1.0f && !isPlaying)
        {
            StartCoroutine(coroutine);
            coroutine = PlayHeavyBreath();
        }
    }

    private IEnumerator PlayHeavyBreath()
    {
        isPlaying = true;
        var playTime = clip.length;
        audioSource.PlayOneShot(clip);
        yield return new WaitForSeconds(playTime);
        isPlaying = false;
    }
}