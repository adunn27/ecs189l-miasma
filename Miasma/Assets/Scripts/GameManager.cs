using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Miasma;

public class GameManager : MonoBehaviour
{
    [SerializeReference] private GameObject Generator1;
    [SerializeReference] private GameObject Generator2;
    [SerializeReference] private GameObject Generator3;

    [System.NonSerialized] public GeneratorController[] GeneratorControllerList;

    // Start is called before the first frame update
    void Start()
    {
        this.GeneratorControllerList = new GeneratorController[3];
        this.GeneratorControllerList[0] = Generator1.GetComponent<GeneratorController>();
        this.GeneratorControllerList[1] = Generator2.GetComponent<GeneratorController>();
        this.GeneratorControllerList[2] = Generator3.GetComponent<GeneratorController>();
    }

    // Get Active Status of Generator 1-3
    public bool checkStatus(int genNum)
    {
        return this.GeneratorControllerList[genNum - 1].getActiveStatus();
    }
}
