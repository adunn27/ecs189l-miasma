using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Miasma;
public class AltarInteractable : MonoBehaviour, IInteractable
{
    private ParticleSystem particle;
    private Light candleLight;
    private bool isEnabled = false;
    private bool firstAccess = true;
    [SerializeField] private int AltarNumber;
    [SerializeField] private RespawnController RespawnManager;
    [SerializeField] private UISceneManager SceneManager;

    public string InteractionText{get;} = "Press E to Activate";

    // Start is called before the first frame update
    void Start()
    {
        particle = GetComponentInChildren<ParticleSystem>();
        candleLight = GetComponentInChildren<Light>();
        particle.Stop();
    }

    public void Interact()
    {
        if (firstAccess)
        {
            firstAccess = false;
            if (AltarNumber == 0)
            {
                SceneManager.startCutscene(0);
            }
        }

        if (isEnabled == false)
        {
            isEnabled = true;
            particle.Play();
            candleLight.enabled = true;
            RespawnManager.SetRespawn(AltarNumber, this);
        }

        // Play the Altar Interaction Audio
        GetComponent<AltarAudio>().PlayAltarClip();
    }

    public float LongInteract(float timeAdded)
    {
        return 0f;
    }

    public bool getActiveStatus()
    {
        return true;
    }

    public void TurnOff()
    {
        isEnabled = false;
        particle.Stop();
        candleLight.enabled = false;
    }
}
