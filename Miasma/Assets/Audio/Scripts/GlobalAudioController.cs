using UnityEngine;
using System.Collections.Generic;


public class GlobalAudioController : MonoBehaviour
{
    private AudioSource[] aSources;
    enum idx
    {
        BGM,
        BGSE
    }

    private void Awake()
    {
        aSources = GetComponents<AudioSource>();
    }

    // Use this public function if you want to adjust the Audio in Options; 
    public void adjust(int i, bool toggle = false, float volume = -1.0f)
    {
        var audioSource = aSources[i];

        if (toggle)
        {
            if (audioSource.isPlaying) {audioSource.Pause();}
            else {audioSource.UnPause();}
        }

        if (volume >= 0.0f)
        {
            audioSource.volume = volume;
        }
    }
}