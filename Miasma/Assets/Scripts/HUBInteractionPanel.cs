using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HUBInteractionPanel : MonoBehaviour
{
    [SerializeField] private GameObject ProgressBar;
    private Slider ProgressBarSlider;
    [SerializeField] private TextMeshProUGUI InteractiveText;
    [SerializeField]private GameObject StoryText;   
    private float currentDuration;

    void Start()
    {
    	ProgressBarSlider = ProgressBar.GetComponent<Slider>();
        ProgressBarSlider.value = 0f;
        ProgressBar.SetActive(false);
        InteractiveText.SetText(""); 
        SetStoryText("");
    }

    public void SetInteractiveText(string message)
    {
    	InteractiveText.SetText(message); 
    }

    /*public void SetMaxOfProgressBar(float max)
    {
        ProgressBarSlider.maxValue = max;
        Debug.Log("SetMaxOfProgressBar: " + max);
    }*/
    public void UpdateProgressBar(float percentage)
    {
    	ProgressBarSlider.value = percentage;
        // Debug.Log("UpdateProgressBar: " + ProgressBarSlider.value/ProgressBarSlider.maxValue);
        if (ProgressBarSlider.value >= 1)
        {
            ResetUI();
        }
    }

    public void ResetUI()
    {
        if (InteractiveText.text != "")
        {
            //ProgressBarSlider.value = 0f;
            ProgressBar.SetActive(false);
            InteractiveText.SetText(""); 
        }  
    }

    public void ActivateProgressBar()
    {
        ProgressBar.SetActive(true);
    }
    public void DeactivateProgressBar()
    {
        ProgressBar.SetActive(false);
        //ProgressBarSlider.value = 0f;
    }

    public void SetStoryText(string message)
    {
        StoryText.GetComponent<TextMeshProUGUI>().text = message;
    }
}
