using UnityEngine;
using System.Collections;

public class AltarAudio : MonoBehaviour
{
    private AudioSource audioSource;
    [SerializeField] private AudioClip clip;
    private IEnumerator coroutine;
    private bool isPlaying = false;
    
    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        coroutine = Play();
    }

    public void PlayAltarClip()
    {
        if (!isPlaying) {
            StartCoroutine(coroutine);
            coroutine = Play();
        }
    }

    private IEnumerator Play()
    {
        isPlaying = true;
        audioSource.PlayOneShot(clip);
        yield return new WaitForSeconds(clip.length);
        isPlaying = false;
    }
    
}