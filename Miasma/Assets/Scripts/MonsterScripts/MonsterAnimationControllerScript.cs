using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterAnimationControllerScript : MonoBehaviour
{
    [SerializeReference] private Animator animator;
    [SerializeReference] private float walkingSensitivity;
    // Start is called before the first frame update
    private Vector3 CurrentPos;
    void Start()
    {
        CurrentPos = transform.position;
    }

    // Update is called once per framed
    void Update()
    {
        float distanceTraveled = Vector3.Distance(CurrentPos, transform.position);
        CurrentPos = transform.position;
        if (distanceTraveled >= walkingSensitivity)
        {
            animator.SetBool("isWalking", true);
        }
        else
        {
            animator.SetBool("isWalking", false);
        }
    }
}
