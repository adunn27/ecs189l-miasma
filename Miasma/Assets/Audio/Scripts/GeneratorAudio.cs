using UnityEngine;
using System.Collections;

public class GeneratorAudio : MonoBehaviour
{
    private AudioSource audioSource;
    private GeneratorController generatorController;
    private bool deactivated = false;
    private bool playingFixingMusic = false;
    [SerializeField] private AudioClip generatorPowerDownSound;
    [SerializeField] private AudioClip generatorRunningSound;
    [SerializeField] private AudioClip generatorFixingSound;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = generatorRunningSound;
        audioSource.Play();
        generatorController = GetComponent<GeneratorController>();
    }

    private void Update()
    {
        var isActive = generatorController.getActiveStatus();
        var isFixing = generatorController.getFixingStatus();

        // Generator being fixed but the Fixing Music is not being played.
        if (isActive && isFixing && !playingFixingMusic)
        {
            playingFixingMusic = true;
            audioSource.clip = generatorFixingSound;
            audioSource.Play();
        }

        // Generator still Active and no longer being fixed.  
        if (isActive && !isFixing && playingFixingMusic)
        {
            playingFixingMusic = false;
            audioSource.clip = generatorRunningSound;
            audioSource.Play();
        }

        // Generator deactivated. 
        if (!isActive && !deactivated)
        {
            deactivated = true;
            StartCoroutine(PowerDown());
        }
    }

    private IEnumerator PowerDown()
    {
        var playTime = generatorPowerDownSound.length;
        audioSource.clip = null;
        audioSource.PlayOneShot(generatorPowerDownSound);
        yield return new WaitForSeconds(playTime);
        audioSource.Stop();
        this.enabled = false;
    }

}